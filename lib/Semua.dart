import 'package:flutter/material.dart';

class Semua extends StatelessWidget {
  const Semua({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Semua Presensi'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[
            Text('Semua Presensi Karyawan RSIA SAMUDRA HUSADA'),
          ],
        ),
      ),
    );
  }
}
