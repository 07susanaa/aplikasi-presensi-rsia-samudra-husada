import 'package:flutter/material.dart';
import 'package:splash_screen/home.dart';
import 'package:splash_screen/splashscreen.dart';
import 'package:splash_screen/page/Dashboard.dart';
import 'package:splash_screen/page/Profil.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SplashScreen(),
      debugShowCheckedModeBanner: false,
    );
  }
}
