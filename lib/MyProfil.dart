import 'package:flutter/material.dart';

class MyProfil extends StatefulWidget {
  MyProfil({Key? key}) : super(key: key);

  @override
  _MyProfil createState() => _MyProfil();
}

class _MyProfil extends State<MyProfil> {
  bool isObscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Edit Profil'),
        ),
        body: Container(
            padding: EdgeInsets.only(left: 15, top: 20, right: 15),
            child: GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: ListView(
                children: [
                  Center(
                    child: Stack(
                      children: [
                        Container(
                          width: 130,
                          height: 130,
                          decoration: BoxDecoration(
                              border: Border.all(width: 4, color: Colors.white),
                              boxShadow: [
                                BoxShadow(
                                    spreadRadius: 2,
                                    blurRadius: 10,
                                    color: Colors.black.withOpacity(0.1))
                              ],
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: AssetImage('images/profil_anda.png'))),
                        ),
                        Positioned(
                          bottom: 0,
                          right: 0,
                          child: Container(
                            height: 40,
                            width: 40,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                border:
                                    Border.all(width: 4, color: Colors.white),
                                color: Colors.blue),
                          ),
                        ),
                      ],
                    ),
                  ),
                  Padding(padding: const EdgeInsets.only(top: 20.0)),
                  Container(
                    width: 380,
                    child: Card(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0)),
                      color: Colors.white,
                      child: Column(
                        children: <Widget>[
                          Text(
                            'My Profil',
                            style: TextStyle(
                                fontSize: 24, fontWeight: FontWeight.bold),
                          ),
                          ...ListTile.divideTiles(color: Colors.grey, tiles: [
                            ListTile(
                              leading: Icon(Icons.account_circle),
                              title: Text(
                                'Nama',
                                style: TextStyle(fontSize: 20),
                              ),
                              subtitle: Text(
                                'Dwi Susanti',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.calendar_today),
                              title: Text(
                                'Tempat & Tanggal Lahir',
                                style: TextStyle(fontSize: 20),
                              ),
                              subtitle: Text(
                                'Magetan, 10 Juli 2001',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.email),
                              title: Text(
                                'Email',
                                style: TextStyle(fontSize: 20),
                              ),
                              subtitle: Text(
                                'dwisusanti@gmail.com',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.phone),
                              title: Text(
                                'No. Telephone',
                                style: TextStyle(fontSize: 20),
                              ),
                              subtitle: Text(
                                '089778987986',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                            ListTile(
                              leading: Icon(Icons.home),
                              title: Text(
                                'Alamat',
                                style: TextStyle(fontSize: 20),
                              ),
                              subtitle: Text(
                                'Ds. Buluarjo RT 27 / RW 04, Kec. Plaosan, Kab. Magetan, Jawa Timur',
                                style: TextStyle(fontSize: 18),
                              ),
                            ),
                          ])
                        ],
                      ),
                    ),
                  )
                ],
              ),
            )));
  }
}
